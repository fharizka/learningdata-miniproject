import React from "react";
import { Link, Route, Switch } from "react-router-dom";
import Register from "./pages/Register";
import Review from "./pages/Review";
import Home from "./pages/Home"

const App = () => {
  return (
    <>
      <ul>
      <li>
          <Link to="/">Home</Link>
        </li>
        <li>
          <Link to="/register">Register</Link>
        </li>
        <li>
          <Link to="/review">Review</Link>
        </li>
      </ul>
      <hr />
    

    <Switch>
      <Route path= "/review">
        <Review />
      </Route>
      <Route path= "/register">
        <Register />
      </Route>
      <Route path= "/">
        <Home />
      </Route>
    </Switch>

    </>
  )
}

export default App