import React, { useState, useEffect } from "react";
import axios from "axios";

const Review = () => {
  const [rating, setRating] = useState(0);
  const [review, setReview] = useState("");
  const [reviewData, setReviewData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    getData();
    setIsLoading(false);
  }, []);

  // function untuk get data
  const getData = () => {
    axios
      .get("https://5fa4bcd2732de900162e85ef.mockapi.io/api/reviews")
      .then((res) => {
        setReviewData(res.data);
      });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setIsLoading(true);

    const data = {
      rating: rating,
      review: review
    };
    axios
      .post("https://5fa4bcd2732de900162e85ef.mockapi.io/api/reviews", data)
      .then((res) => {
        console.log(res);
        getData();
        setIsLoading(false);
      })
      .catch((err) => console.log(err));
  };

  const handleDelete = (e, id) => {
    e.preventDefault();
    setIsLoading(true);

    const url = "https://5fa4bcd2732de900162e85ef.mockapi.io/api/reviews";
    axios.delete(`${url}/${id}`).then((res) => {
      getData();
      setIsLoading(false);
    });
  };

  return (
    <>
      <form onSubmit={handleSubmit}>
        Rating:
        <br />
        <select value={rating} onChange={(e) => setRating(e.target.value)}>
          <option value="0">Select Rating</option>
          <option value="1">1</option>
          <option value="2">2</option>
          <option value="3">3</option>
          <option value="4">4</option>
          <option value="5">5</option>
        </select>
        <br />
        <br />
        Review:
        <br />
        <textarea onChange={(e) => setReview(e.target.value)}></textarea>
        <br />
        <br />
        <button type="submit">Submit Review</button>
        <hr />
        {isLoading && <p>Loading Data...</p>}
        {!isLoading &&
          reviewData.map((data) => (
            <div key={data.id}>
              Id: {data.id}
              <br />
              Rating: {data.rating} <br />
              Review: {data.review} <br />
              <button onClick={(e) => handleDelete(e, data.id)}>Hapus</button>
              <hr />
            </div>
          ))}
      </form>
    </>
  );
};

export default Review;
