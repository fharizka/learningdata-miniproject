import React, { useState } from "react";

const Register = () => {
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [email, setEmail] = useState("")
    const [gender, setGender] = useState("")
    const [password, setPassword] = useState("")

    const handleSubmit = (e)=> {
        e.preventDefault()

        const url = "https://5fa4bcd2732de900162e85ef.mockapi.io/api/register"
        const bodyData = {
            firstName: firstName,
            lastName: lastName,
            email: email,
            gender: gender,
            password: password
        }

        fetch(url, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(bodyData)
        })

            .then((res) => res.json())
            .then((res) => console.log(res))
    }

    return (
        <div className="App">
            <form onSubmit={handleSubmit}>
                
            <div className="form-group">
                <label>First Name</label>
                <br/>
                <input type="text" placeholder="Enter first name" onChange={(e) => setFirstName(e.target.value)} />
            </div>

            <div className="form-group">
                <label>Last Name</label>
                <br/>
                <input type="text" placeholder="Enter last name" onChange={(e) => setLastName(e.target.value)}  />
            </div>

            <div className="form-group">
                <label>Email</label>
                <br/>
                <input type="email" placeholder="Enter email" onChange={(e) => setEmail(e.target.value)} />
            </div>

            <div className="form-group">
                <label>Gender</label>
                <br/>
                <input type="radio" name="gender" value="Lakilaki" onChange={(e) => setGender(e.target.value)} /> Laki-laki
                <input type="radio" name="gender2" value="Perempuan" onChange={(e) => setGender(e.target.value)} /> Perempuan
            </div>

            <div className="form-group">
                <label>Password</label>
                <br/>
                <input type="password" placeholder="Enter password" onChange={(e) => setPassword(e.target.value)}  />
            </div>

                <button type="submit" >Sign Up</button>
            </form>
        </div>
    
    )
}

export default Register